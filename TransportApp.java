/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportapp;

import factorymethod.VehiculoFactory;
import factorymethod.VehiculoFactoryMethod;

/**
 *
 * @author LENOVO
 */
public class TransportApp {

    /*
    **Posibles variables de entrada del Usuario
    */
    int xuser;
    int yuser;
    int zuser;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /*
        **Llamada para crear el vehiculo
        */
        VehiculoFactoryMethod factory= new VehiculoFactory();
        //Estoy creando un Bus
        Vehiculo vehiculo =factory.createVehiculo(0, 0, 0, 0, true);
        System.out.println(vehiculo.getNombre()+vehiculo.getMovimientox()
                +","+vehiculo.getMovimientoy()+","+vehiculo.getMovimientoz()+",Tiempo:"+
                vehiculo.getTiempo()+",Parada:"+vehiculo.getParada());
        //Estoy creando un Avion
        Vehiculo vehiculo2 =factory.createVehiculo(0, 0, 2, 0, true);
        System.out.println(vehiculo2.getNombre()+vehiculo2.getMovimientox()
                +","+vehiculo2.getMovimientoy()+","+vehiculo2.getMovimientoz()+",Tiempo:"+
                vehiculo2.getTiempo()+",Parada:"+vehiculo2.getParada());
    }
    
}
