### Patrones de Dise�o###
Ejercicio: Dado un problema implementar la soluci�n haciendo uso de cuatro patr�nes de Dise�o de Software.

### Problema ###
La situacion problema que se resolvera con el dise�o es la siguiente:

Los usuarios de Transmilenio y de la aerolinea Avianca desean conocer cuando su ruta de inter�s ha llegado a su paradero, para ellos no perder tiempo en la estacion o en el aeropuerto.

### Soluci�n ###

La solucion que se plantea es hacer la implementaci�n de los patrones de dise�o Iterator, Factory Method, Observer y Memento. El usuario va a realizar la suscripci�n a la ruta que desee tomar bien sea ruta terrestre o ruta aerea, para tal fin se usar� el patr�n Observer que dada esa suscripci�n va a poder observar cuando la ruta llegue a la parada deseada va a notificar al usuario que dicha ruta ya esta en la parada. La implementaci�n de Factory Method se realiza con el objetivo de crear VehiculosAereos y VehiculosTerrestre que contienen diferentes implementaciones, dado que se van a manejar vehiculos de tipo buseta (terrestre) y vehiculos de tipo avion (aereo). El patr�n Iterator cumple la funci�n de a partir de las rutas establecidas por Transmilenio y por Avianca poder empezar a Iterar sobre cada una de las paradas que cumple cada una de dichas rutas. Y a partir de las paradas iteradas con el patr�n anterior, el patr�n Memento guardara la posici�n por la que ha pasado la ruta.

### Modelo ###

* Se encuentra en Diagrama de /Diagrama_de_Clases.jpg


### Integrantes ###

* 20141020053 Johan Sebastian Bonilla Plata
* 20141020096 Javier Duvan Hospital Melo
* 20141020135 Miguel �ngel Hern�ndez Cubillos


