package memento;
import transportapp.Parada;

public class Memento {
    private Parada estado;
 
    public Memento(Parada estado){
       this.estado = estado;
    }
 
    public Parada getEstado(){
       return this.estado;
    }	
 }