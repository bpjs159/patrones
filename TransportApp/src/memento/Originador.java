package memento;
import transportapp.Parada;

public class Originador {
    private Parada estado;
 
    public void setEstado(Parada estado){
       this.estado = estado;
    }
 
    public Parada getEstado(){
	    return this.estado;
    }
 
    public Memento saveEstadoToMemento(){
       return new Memento(this.estado);
    }
 
    public void setEstadoMemento(Memento memento){
       this.estado = memento.getEstado();
    }
 }