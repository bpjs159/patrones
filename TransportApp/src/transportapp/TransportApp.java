/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportapp;

 
import observer.IPasajero;
import observer.ISujeto;
import observer.Sujeto;
import observer.Pasajero;

import factorymethod.VehiculoAereo;
import factorymethod.VehiculoTerrestre;
import factorymethod.VehiculoFactory;
import factorymethod.VehiculoFactoryMethod;

import memento.CareTaker;
import memento.Memento;
import memento.Originador;


/**
 *
 * @author LENOVO
 */
public class TransportApp{

    /*
    **Posibles variables de entrada del Usuario
    */
    int xuser;
    int yuser;
    int zuser;


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /*
        **Llamada para crear el vehiculo
        */
        
        System.out.println("CREACIÓN DE VEHICULOS");

        VehiculoFactoryMethod factory= new VehiculoFactory();
        //Estoy creando un Bus
        VehiculoTerrestre vehiculo =factory.createVehiculoTerrestre(0, 0, 0, true,null);
        System.out.println(vehiculo.getNombre()+vehiculo.getMovimientox()
                +","+vehiculo.getMovimientoy()+",Tiempo:"+
                vehiculo.getTiempo()+",Parada:"+vehiculo.getParada());
        //Estoy creando un Avion
        VehiculoAereo vehiculo2 =factory.createVehiculoAereo(0, 0, 0, 0, true,null);
        System.out.println(vehiculo2.getNombre()+vehiculo2.getMovimientox()
                +","+vehiculo2.getMovimientoy()+","+vehiculo2.getMovimientoz()+",Tiempo:"+
                vehiculo2.getTiempo()+",Parada:"+vehiculo2.getParada());

        System.out.println("--------------------------------");  

        //crear e insertar rutas
        //para B14

        Parada paradaB141 = new Parada("Calle 45",0,0);
        Parada paradaB142 = new Parada("Marly",0,5);
        Parada paradaB143 = new Parada("Calle 57",0,10);
        Parada paradaB144 = new Parada("Calle 63",0,10);

        Ruta B14 = new Ruta("B14");
        B14.agregarParada(paradaB141);
        B14.agregarParada(paradaB142);
        B14.agregarParada(paradaB143);
        B14.agregarParada(paradaB144);

        vehiculo.setRuta(B14);


        //crear e insertar rutas
        //para alemania

        Parada paradaAlemania1 = new Parada("Bogota",0,0);
        Parada paradaAlemania2 = new Parada("Medellin",0,5000);
        Parada paradaAlemania3 = new Parada("Madrid",0,10000);
        Parada paradaAlemania4 = new Parada("Munich",0,15000);

        Ruta Alemania = new Ruta("Alemania");
        Alemania.agregarParada(paradaAlemania1);
        Alemania.agregarParada(paradaAlemania2);
        Alemania.agregarParada(paradaAlemania3);
        Alemania.agregarParada(paradaAlemania4);
        
        vehiculo2.setRuta(Alemania);
        

        //Subscripcion de pasajeros
        ISujeto sujeto = new Sujeto();
        IPasajero pasajero1 = new Pasajero();
        IPasajero pasajero2 = new Pasajero();
        IPasajero pasajero3 = new Pasajero();
       
        System.out.println("SUSCRIPCIÓN DE PASAJAEROS");

        
        sujeto.suscribirse(pasajero1);
        System.out.println("El pasajero 1 ha realizado la suscripción a la Ruta de TM B14, en la parada Marly");
        sujeto.suscribirse(pasajero2);
        System.out.println("El pasajero 2 ha realizado la suscripción a la Ruta de TM B14, en la parada Calle 57");
        sujeto.suscribirse(pasajero3);
        System.out.println("El pasajero 3 ha realizado la suscripción a la Ruta de Avianca que cubre Bogotá - Munich, en la escala de Medellín");
        
        
        
        pasajero1.setParada(paradaB142);
        pasajero1.setNombre("Pasajero 1.. A la estación Marly");
        pasajero2.setParada(paradaB143);
        pasajero2.setNombre("Pasajero 2.. A la estación Calle57");
        pasajero3.setParada(paradaAlemania2);
        pasajero3.setNombre("Pasajero 3.. Al aeropuerto Medellin");

        
        
        System.out.println("--------------------------------");  

       //avanzar rutas
        System.out.println("ITERACIÓN DE RUTAS TERRESTRES");

        vehiculo.setSujeto(sujeto);
        
        vehiculo.avanzar(); //Calle45 -> Marly
        System.out.println("La parada en la que se encuentra el bus es:  "+vehiculo.actual().getParada());
        vehiculo.avanzar(); //Marly -> Calle 57
        System.out.println("La parada en la que se encuentra el bus es: "+vehiculo.actual().getParada());
        vehiculo.avanzar(); //Calle 57 -> Calle 63
        System.out.println("La parada en la que se encuentra el bus es: "+vehiculo.actual().getParada());
        
        System.out.println("--------------------------------");   
        
        System.out.println("ITERACIÓN DE RUTAS AEREAS");

        
        vehiculo2.setSujeto(sujeto);
        vehiculo2.avanzar();
        System.out.println("La parada en la que se encuentra el avion es: "+" "+vehiculo2.actual().getParada());
        vehiculo2.avanzar(); //Marly -> Calle 57
        System.out.println("La parada en la que se encuentra el avion es: "+" "+vehiculo2.actual().getParada());
        vehiculo2.avanzar(); //Calle 57 -> Calle 63
        System.out.println("La parada en la que se encuentra el avion es: "+" "+vehiculo2.actual().getParada());

        System.out.println("--------------------------------");       
        
        System.out.println("GUARDADO DE PARADAS");


        //Memento, devolviendo al estado de ruta 0
        vehiculo.setEstadoRuta(0);
        System.out.println("La parada del bus guardada fue:  "+vehiculo.actual().getParada());
        
        vehiculo2.setEstadoRuta(0);
        System.out.println("La parada del avion guardada fue:  "+vehiculo2.actual().getParada());



        







        








        




    }
    
}
