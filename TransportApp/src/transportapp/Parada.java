package transportapp;
/**
 *
 * @author Sebastian
 */
public class Parada{

    String parada;
    int x;
    int y;

    public Parada(String parada, int x, int y){
        this.parada = parada;
        this.x = x;
        this.y = y;
    }

    public String getParada(){
        return this.parada;
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    
}