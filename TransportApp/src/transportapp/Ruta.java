package transportapp;
import java.util.ArrayList;
import transportapp.Parada;
import iterator.*;
/**
 *
 * @author Sebastian
 */
public class Ruta{

    String ruta;
    AgregadoConcreto agregado = new AgregadoConcreto();
    Iterador iterador;

    public Ruta(String ruta){
        this.ruta = ruta;
        iterador = agregado.getIterador();
    }

    public String getRuta(){
        return this.ruta;
    }

    public void agregarParada(Parada parada){
        this.agregado.agregar(parada);
    }

    

    public Parada paradaActual(){
        return iterador.actual();
    }

    public void siguienteParada(){
        iterador.siguiente();
    }

    public void actualizarParada(int indice){
        iterador.indice(indice);
    }




}