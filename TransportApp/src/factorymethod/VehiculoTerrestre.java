/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

import transportapp.Ruta;
import transportapp.Parada;
import observer.ISujeto;

import memento.Originador;
import memento.CareTaker;
/**
 *
 * @author LENOVO
 */
public abstract class VehiculoTerrestre {
    /*
    *Creacion de variables de movx, movy y movz, tiempo y parada
    */
    private int movx;
    private int movy;
    private int tiempo;
    private boolean parada;
    private Ruta ruta;
    private ISujeto sujeto;

    //Memento

    Originador originator = new Originador();
    CareTaker careTaker = new CareTaker();
    /*
    *Set de las variable
    */      
    public VehiculoTerrestre(int movx,int movy,int tiempo, boolean parada, Ruta ruta){
        setMovx(movx);
        setMovy(movy);
        setTiempo(tiempo);
        setParada(parada);
        setRuta(ruta);
    }
    /*
    *Creacion de Metodos Abstractos
    */
    public abstract int getMovimientox();
    public abstract int getMovimientoy();
    public abstract int getTime();
    public abstract boolean getEstadoParada();
    public abstract String getNombre();
    
    /*
    *Generacion de metodos Get and Set
    */

    public int getMovx() {
        return movx;
    }

    public void setMovx(int movx) {
        this.movx = movx;
    }

    public int getMovy() {
        return movy;
    }

    public void setMovy(int movy) {
        this.movy = movy;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }
    
     public boolean getParada() {
        return parada;
    }

    public void setParada(boolean parada) {
        this.parada = parada;
    }

    public Ruta getRuta() {
        return ruta;
    }

    public void setRuta(Ruta ruta) {
        this.ruta = ruta;
    }

    public void setSujeto(ISujeto sujeto) {
        this.sujeto = sujeto;
    }

    public void avanzar() {


        this.originator.setEstado(this.ruta.paradaActual());
        careTaker.agregar(this.originator.saveEstadoToMemento());

        this.ruta.siguienteParada();
        if(this.sujeto!=null)
        this.sujeto.notificar(this.actual());
    }

    public void setEstadoRuta(int indice){
        originator.setEstadoMemento(careTaker.obtener(indice));
        this.ruta.actualizarParada(indice);
        
    }

    public Parada actual() {
        return this.ruta.paradaActual();
    }
    
    
    
}
