/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;
import transportapp.Ruta;
/**
 *
 * @author LENOVO
 */
public class VehiculoFactory implements VehiculoFactoryMethod{
    
   /*
    *Aqui se esta creando el vehiculo se define un vehiculo cuando su z =0 y un avion cuando su z>1
    *Las paradas estan en false debido a que esta quieto
    */

    /**
     *
     * @param movx
     * @param movy
     * @param movz
     * @param tiempo
     * @param parada
     * @param ruta
     * @return
     */
    
    
    @Override
    public VehiculoTerrestre createVehiculoTerrestre(int movx, int movy,int tiempo, boolean parada, Ruta ruta){
        if((movx>=0) && (movy>=0) && (tiempo>=0)&& (parada==true)){
            return new Bus(movx,movy,tiempo,parada,ruta);
        }
        
        return null;
    }
    
    
    
    @Override
    public VehiculoAereo createVehiculoAereo(int movx, int movy, int movz, int tiempo, boolean parada, Ruta ruta){
       if((movx>=0) && (movy>=0) && (movz==0)&& (tiempo>=0)&& (parada==true)){
            return new Avion(movx,movy,movz,tiempo,parada,ruta);
        }
        return null;
    }
}
