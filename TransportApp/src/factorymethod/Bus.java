/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;
import transportapp.Ruta;

/**
 *
 * @author LENOVO
 */
public class Bus extends VehiculoTerrestre {
    
    int x2;
    int y2;
    int z2;
    int t2;
    boolean p2;
    public Bus(int x, int y,int t, boolean p, Ruta r){
        super(x,y,t,p,r);
        x2=x;
        y2=y;
        t2=t;
        p2=p;
    }
    /*
    **Suma los movimientos en c, y y z y ademas suma el tiempo, la idea es hacerlo con iterator
    */
    @Override
    public int getMovimientox(){
        return (x2+1);
    }
    @Override
    public int getMovimientoy(){
        return (y2+1);
    }
    @Override
    public int getTime(){
        return (t2+1);
        
    }
    
    /*
    *Simplemente obtiene el estado de la parada cuando los x, y y z estan cerca
    */
    
    @Override
    public boolean getEstadoParada(){
        return p2;
        
    }
    
    @Override
    public String getNombre(){
        return "Bus Creado en:";
        
    }
}