/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;
import transportapp.Ruta;
/**
 *
 * @author LENOVO
 */
public interface VehiculoFactoryMethod {
    public VehiculoTerrestre createVehiculoTerrestre(int movx,int movy,int tiempo,boolean parada,Ruta ruta);
    public VehiculoAereo createVehiculoAereo(int movx,int movy,int movz,int tiempo,boolean parada,Ruta ruta);
}
