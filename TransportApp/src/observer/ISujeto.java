package observer;
import transportapp.Parada;

/**
 *
 * @author Sebastian
 */
public interface ISujeto {
    public void suscribirse(IPasajero observador);
    public void desSuscribirse(IPasajero observador);
    public void notificar(Parada vehiculoLlega);
}