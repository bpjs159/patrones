package observer;
import java.util.ArrayList;
import transportapp.Parada;
/**
 *
 * @author Sebastian
 */
public class Sujeto implements ISujeto{

    //observer
    private static ArrayList<IPasajero> observadores = new ArrayList<IPasajero>();

    public void suscribirse(IPasajero observador){
        observadores.add(observador);
    };
    public void desSuscribirse(IPasajero observador){
        observadores.remove(observador);
    };
    public void notificar(Parada vehiculoLlega){
        for(int i=0;i<observadores.size();i++){
            if(observadores.get(i).getParada()==vehiculoLlega)
            observadores.get(i).actualizar();
        }
    };
}