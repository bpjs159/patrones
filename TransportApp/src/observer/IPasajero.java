package observer;
import transportapp.Parada;

/**
 *
 * @author Sebastian
 */
public interface IPasajero {
    public void actualizar();
    public void setParada(Parada parada);
    public Parada getParada();
    public void setNombre(String nombre);
}