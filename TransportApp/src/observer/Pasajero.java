package observer;
import observer.IPasajero;
import transportapp.Parada;
/**
 *
 * @author Sebastian
 */
public class Pasajero implements IPasajero {
    Parada miParada;
    String nombre = "";

    public void setParada(Parada parada){
        this.miParada = parada;
    }

    public Parada getParada(){
        return this.miParada;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void actualizar(){
        System.out.println("Ha llegado el transporte para " + this.nombre);
    }
}