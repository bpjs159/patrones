package iterator;
import transportapp.Parada;
import java.util.Vector;

public class AgregadoConcreto implements Agregado
{
    protected Vector <Parada> aDatos = new Vector<Parada>();

    

    @Override
    public Iterador getIterador()
    {
        return new IteradorConcreto( this );
    }

    

    public void agregar(Parada parada)
    {
        this.aDatos.add(parada);
    }
}