package iterator;
import transportapp.Parada;

public interface Iterador
{
    public Parada primero();
    public Parada siguiente();
    public boolean hayMas();
    public Parada actual();
    public void indice(int indice);
}